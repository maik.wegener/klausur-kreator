#!/bin/bash
cat ../template/header.tex > klausur.tex

files=(aufgaben/*)
for i in {1..4}
do
   taskPath=$(printf "%s\n" "${files[RANDOM % ${#files[@]}]}")
   echo "\section*{Aufgabe $i}" >> klausur.tex
   echo "\begin{figure}[tbh]
	        \includegraphics[width=\linewidth]{$taskPath}
         \end{figure}" >> klausur.tex 
done

echo '\end{document}' >> klausur.tex
