## Mehrfachaufgaben in den Altklausuren

Folgende Aufgaben kommen mehrfach in den Klausuren vor:

Legende: 
* =: exakt gleich
* ≈: sehr ähnlich

Liste der Aufgaben:
* SS_2008_2 = SS_2008_nach_2 ≈ SS_2009_1 
* SS_2008_4 = SS_2010_nach_7 ≈ SS_2012_nach_6 ≈ SS_2013_nach_5
* SS_2008_5 = SS_2012_6
* SS_2008_6 = SS_2008_nach_6 = SS_2010_6 = SS_2011_nach_3 = SS_2012_5
* SS_2008_7 ≈ SS_2008_nach_7
* SS_2008_nach_1 = SS_2012_nach_1 ≈ SS_2011_nach_1 = SS_2013_2
* SS_2008_nach_3 = SS_2011_nach_2
* SS_2008_nach_5 = SS_2011_nach_6
* SS_2008_nach_8 = SS_2010_7 = SS_2012_2 
* SS_2009_2 = SS_2014_3
* SS_2009_3/4 ≈ SS_2010_nach_5 = SS_2014_5 ≈ SS_2012_nach_4
* SS_2009_5 = SS_2010_nach_6 
* SS_2009_6 = SS_2010_nach_8
* SS_2009_nach_1 = SS_2015_6
* SS_2009_nach_4 = SS_2011_nach_4 
* SS_2010_1 = SS_2011_nach_5 
* SS_2010_2 ≈ SS_2014_1
* SS_2010_3 = SS_2014_2
* SS_2010_4 = SS_2012_nach_2 ≈ SS_2013_3
* SS_2010_nach_1 = SS_2013_1 = SS_2015_1
* SS_2010_nach_2 = SS_2012_1
* SS_2010_nach_3 = SS_2012_nach_3
* SS_2010_nach_4 = SS_2013_5
* SS_2012_nach_5 = SS_2013_nach_2
* SS_2013_6 = SS_2015_4
* SS_2013_nach_1 = SS_2015_5
* SS_2013_nach_3 = SS_2014_2
* SS_2013_nach_4 = SS_2015_3

