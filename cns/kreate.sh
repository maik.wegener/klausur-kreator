#!/bin/bash
cat ../template/header.tex > klausur.tex

for task in aufgaben/*.png; do
	klausur=$(echo "$task" | grep -oP '\d.*(?=_)' | sed 's/_/ /g') 
	tasknr=$(echo "$task" | grep -oP '(?<=_)\d\.' | sed 's/\.//g') 
	echo "\section*{Klausur $klausur, Aufgabe $tasknr}" >> klausur.tex
    echo "\maxsizebox{0.9\linewidth}{0.9\paperheight}{\includegraphics[width=\linewidth]{$task}}" >> klausur.tex 
	echo '\newpage' >> klausur.tex
done

echo '\end{document}' >> klausur.tex
