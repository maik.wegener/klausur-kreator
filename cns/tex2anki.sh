#!/bin/bash
filename="klausur.tex"
while read -r line
do
    case "$line" in
        *.png*) echo -n "<img src=\"$(echo "$line" | grep -oP '(?<=]{).*(?=}})')\"/>; [latex]"   ;;
        *\\section*) echo -n "$(echo "$line" | grep -oP '(?<={).*(?=})'):<br>"   ;;
        *\\newpage*) echo "[/latex]" ;;
        *) echo -n "$line ";
        
    esac

done < "$filename"
